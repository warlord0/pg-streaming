#!/bin/bash

docker-compose stop standby
docker-compose run -u postgres --rm standby rm -r /var/lib/postgresql/data/
docker-compose run -u postgres --rm standby pg_basebackup -h master -P -D /var/lib/postgresql/data/
docker-compose run -u postgres --rm standby mv /var/lib/postgresql/data/recovery.done /var/lib/postgresql/data/recovery.conf
docker-compose start standby
