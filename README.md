# Sample of Using docker-compose for PostgreSQL Replication

## docker-compose.yaml

This file builds two PostgreSQL containers, master and standy and exposes the ports for each instance to the host.

It is not a practical set of containers as you'd never really operate anything like this in test or production. It's merely a learning exercise for how to configure replication between two PostgreSQL hosts.

## Usage

Bring up the master and slave containers, and it will create the data structure and populate a default database as per the maintainers init script.

```shell
$ docker-composer up -d
```
When it's up we want to take it all back down again. We only wanted it to create the data folder.

Copy the files from the `./src` folder into the containers data folder. This is a mapped folder so accessible from our shell, but will require `sudo` because of dockers folder permissions.

```shell
$ sudo cp ./src ./masters/data/
```

Now bring the containers up again.

What now needs to happen is the standby must be stopped, a copy of the masters data and configs is copied to the standby and then it is brought back online. This uses pg_basebackup which is scripted in `init-dr.sh`.

```shell
$ sh ./init-dr.sh
```

Check that the replication has started by looking in the logs:

```shell
$ docker-compose logs standby
```

and we should expect to see:
```log
standby    | 2020-01-15 15:53:21.218 UTC [1] LOG:  database system is ready to accept read only connections
standby    | 2020-01-15 15:53:21.242 UTC [29] LOG:  started streaming WAL from primary at 0/A000000 on timeline 1
```

You can test by creating databases on the master and seeing them appear on the standby:

```shell
$ docker-compose exec -u postgres db createdb testdb
$ docker-compose exec -u postgres standby psql -l
```
